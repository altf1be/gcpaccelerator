# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.4.1](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.4.0...v1.4.1) (2020-04-10)


### Documentations

* add article 'web scraping is now legal' ([26b7f6a](https://bitbucket.org/altf1be/gcpaccelerator/commit/26b7f6a4cde8df7effef6321ea869c5a0d608f19))

## [1.4.0](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.3.1...v1.4.0) (2020-04-07)

### [1.3.1](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.3.0...v1.3.1) (2020-04-07)


### Chores

* exclude directories not_used and secret ([10eeeb0](https://bitbucket.org/altf1be/gcpaccelerator/commit/10eeeb04fd266495dcb8980dc25a224c4cbbb986))


### Documentations

* **fix:** the  works not when it is nested by a * (italic) ([bfe2607](https://bitbucket.org/altf1be/gcpaccelerator/commit/bfe26071118c6c91675d98d91fe8ac0feb6b17b3))

## [1.3.0](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.2.2...v1.3.0) (2020-04-07)


### Continous improvements (CI)

* scripts are moved in _scripts, now the user runs build.bat <format> and start.bat <format> to build and open the generated files ([690e873](https://bitbucket.org/altf1be/gcpaccelerator/commit/690e8737282895927f40854b05add89b53862d66))

### [1.2.2](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.2.1...v1.2.2) (2020-04-02)


### Documentations

* adapt the table of contents: remove (is it legal?) and add the list of google' products ([09392d3](https://bitbucket.org/altf1be/gcpaccelerator/commit/09392d3eae0e3346ee2737859e54f75a8beced91))
* add list of google cloud platform' products ([fdb345f](https://bitbucket.org/altf1be/gcpaccelerator/commit/fdb345fa07ca6e009b4c737bc01b8b27bc79cbca))
* **fix:** correct misspellings ([5316a25](https://bitbucket.org/altf1be/gcpaccelerator/commit/5316a25ff166fe60b5bd71678083588e4139cc32))
* add new citation ([7c87c6b](https://bitbucket.org/altf1be/gcpaccelerator/commit/7c87c6bd7f39d79b81cc695caf160578a1ff2a5f))
* add new items in the glossary and correct the styles ([56403a3](https://bitbucket.org/altf1be/gcpaccelerator/commit/56403a33041abd20d7592e075355de8ee178738c))
* add slides Data mining inunusual domainswith information-rich knowledge graph construction,inferenceand search ([24c31a6](https://bitbucket.org/altf1be/gcpaccelerator/commit/24c31a6a776f9c1a3221df7e9701578744c8f82f))
* add slides from scrapinghub presentation : how to scrape the web without being blocked ([ae67fd6](https://bitbucket.org/altf1be/gcpaccelerator/commit/ae67fd61a50f08c091b08ba9c5716f595e129bd8))


### Chores

* rename files ([2c14306](https://bitbucket.org/altf1be/gcpaccelerator/commit/2c14306cc5c3d20d67365a7526c1b41846e11437))
* rename files ([6f8ae26](https://bitbucket.org/altf1be/gcpaccelerator/commit/6f8ae2695534eee090428905dbfb5b96181fd81b))
* settings for pythonpath and restructured  files ([ffdd953](https://bitbucket.org/altf1be/gcpaccelerator/commit/ffdd95315b48a4c983ed7c3ee83cf8ca5fbbb091))

### [1.2.1](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.2.0...v1.2.1) (2020-03-24)


### Chores

* add missing templates to build the docx file ([7448bd1](https://bitbucket.org/altf1be/gcpaccelerator/commit/7448bd1d5e876e223bb62ecff9ba8ded3069572e))


### Documentations

* revise the table of contents and update the glossary and the header with the latest additions ([5a3c917](https://bitbucket.org/altf1be/gcpaccelerator/commit/5a3c9175f35fafd2a4dbcdacf31c2c1109f2876a))
* revise the table of contents and update the glossary and the header with the latest additions ([f06276e](https://bitbucket.org/altf1be/gcpaccelerator/commit/f06276eb0883b3cd79a1609135b27ea3fa996b61))


### Tests

* add a sample of the usage of the scraphinghub.com extract of a product on newpharma.be ([ce0a70a](https://bitbucket.org/altf1be/gcpaccelerator/commit/ce0a70a9c341f4a68446241462869eb749dad605))


### Styles

* improve the style of the graphviz ([6227d63](https://bitbucket.org/altf1be/gcpaccelerator/commit/6227d6359ee07bffc12153d0fd39c5bc6808d1f5))

## [1.2.0](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.1.0...v1.2.0) (2020-03-23)


### Documentations

* add new citations, describe whether the scraping is illegal or not ([0f9e96a](https://bitbucket.org/altf1be/gcpaccelerator/commit/0f9e96aff5d8d7f7126b50a328f11f8288ea774e))

## [1.1.0](https://bitbucket.org/altf1be/gcpaccelerator/compare/v1.0.0...v1.1.0) (2020-03-23)


### Features

* add a chart describing the high level view of the architecture, add a description of the process, update the citations-glossary-header ([49deb60](https://bitbucket.org/altf1be/gcpaccelerator/commit/49deb6024c2e9282b734d45ad2b952303fd7829d))


### Chores

* move images to the directory : gif ([6ba1554](https://bitbucket.org/altf1be/gcpaccelerator/commit/6ba1554f8880200eb377b1ab0add03a5dac99678))
* rename extensions of the images ([45cc9b1](https://bitbucket.org/altf1be/gcpaccelerator/commit/45cc9b1df38dfc2151f7d2e853ece922fa4dc0c6))
* update the title of the project ([111ea0e](https://bitbucket.org/altf1be/gcpaccelerator/commit/111ea0e69886bd1adc46acb88d5020e0624d4713))


### Documentations

* add best practices from scrapinghub.com and extractsummit.io ([68d7f33](https://bitbucket.org/altf1be/gcpaccelerator/commit/68d7f339f28c399808604ad9a59abc7ad499cf24))
* add new terms in the glossary ([4b5b567](https://bitbucket.org/altf1be/gcpaccelerator/commit/4b5b5678d286ea332c8888a1cd581050a312501b))
* clean the documentation ([e13d64f](https://bitbucket.org/altf1be/gcpaccelerator/commit/e13d64fe308a087a87fca455004d45626d1940e7))
* move files into a new directory : best_practices ([02068c8](https://bitbucket.org/altf1be/gcpaccelerator/commit/02068c8de3b312ecaa6b30e74352ffb7f8cd6eef))
* move files into a new directory : research ([4793420](https://bitbucket.org/altf1be/gcpaccelerator/commit/4793420bf8f8d21c38ba818be8782028a0ae6fd9))
* **fix:** move resources presenting the competition to a new directory named ... competition ([9570542](https://bitbucket.org/altf1be/gcpaccelerator/commit/957054200144b647092e06037d3176042190074c))

## 1.0.0 (2020-03-20)


### Features

* add a sample of an export of data from amazon.com from scrapinghub.com ([2fbb09e](https://bitbucket.org/altf1be/gcpaccelerator/commit/2fbb09e596133f686e3864377d67ca05b43ea552))


### Styles

* add logos generated by shopify ([16dba74](https://bitbucket.org/altf1be/gcpaccelerator/commit/16dba749efcc8884109ae9186854102a377d051c))
* the RTD theme has a width of 1024px and a style to strike the text ([462670b](https://bitbucket.org/altf1be/gcpaccelerator/commit/462670b6daaa449d65f6bd1688fb22b01d179fde))


### Chores

* initialize the generation of the documentation for sphinx ([e84019d](https://bitbucket.org/altf1be/gcpaccelerator/commit/e84019d60e8253c21f427b2ef571a2e775442890))
* set gitattributes using https://gitattributes.io/api/common%2Cpython%2Cvisualstudio%2Cweb ([e3751c6](https://bitbucket.org/altf1be/gcpaccelerator/commit/e3751c679aa8121603ccbfb46ca2b903290eff4e))


### Documentations

* add a dashboard showing the usage of the crawlers vs. the prices update in the database. basically, does the crawler work? do we collect prices? ([097f4ba](https://bitbucket.org/altf1be/gcpaccelerator/commit/097f4ba6ccb175c2426206e2471a95a0ba43c61f))
* add a logo of the accelerator generation on https://placeit.net/logo-maker ([42814ab](https://bitbucket.org/altf1be/gcpaccelerator/commit/42814ab679c274d009df8c52316198105f0e24f8))
* add an example of a scraping using a serverless architecture ([d410740](https://bitbucket.org/altf1be/gcpaccelerator/commit/d410740709bbd6cae8b17a858c102f5421a6ba23))
* add data samples from immoffice.be ([ed2f82c](https://bitbucket.org/altf1be/gcpaccelerator/commit/ed2f82c62946e59babbe512d50e2262b9e3943ab))
* add documentation to be inserted in our accelerator from the extractsummit.io ([6d6d1f0](https://bitbucket.org/altf1be/gcpaccelerator/commit/6d6d1f0dcb4c9028f3c720ec3d331d02ca947557))
* add research documents written by Ken van Loon from statbel ([67ecc67](https://bitbucket.org/altf1be/gcpaccelerator/commit/67ecc67a70c9d1932eeb7b009816f34b4cb34d4d))
* add research page concerning the 'Detecting Near-Duplicates for Web Crawling' in real-time during the scraping ([64be20e](https://bitbucket.org/altf1be/gcpaccelerator/commit/64be20e2f45fe67b576fbc729ec8252672b95143))
* add samples from the competitor : eCade.biz ([8d4c0bc](https://bitbucket.org/altf1be/gcpaccelerator/commit/8d4c0bc72ca499528190c49a1f35ba306d9c1423))
* add screenshots of the web app displaying the alerts when a price is manipulated ([7ff115e](https://bitbucket.org/altf1be/gcpaccelerator/commit/7ff115e52909f98b425b7afaa5c0dcaa69033070))
* add slides from diverse sources such as scraphinghub, tutorial concerning the scraping, scraping best practices ([9fc1fa3](https://bitbucket.org/altf1be/gcpaccelerator/commit/9fc1fa32229d3fde795fd26b0a5398093cd51f8c))
* add the documentation of the accelerator: introduction, use cases, capabilities, wrappers description, bi architecture ([0825cb0](https://bitbucket.org/altf1be/gcpaccelerator/commit/0825cb0859a7f85974459bed0cc4223c514ab0a9))
* add the software architecture designed by Gabyygaël ([3ddcfe6](https://bitbucket.org/altf1be/gcpaccelerator/commit/3ddcfe63a58041ac3384aef97818455eab5e59ce))
