.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst

Business Intelligence Architecture
===================================

BI Solution Architecture
-------------------------

The chart describes the interactions between the modules of the Accelerator required to scrap the data until the generation of a BI report.

.. graphviz:: /gcpaccelerator/accelerator-bi_architecture.dot

