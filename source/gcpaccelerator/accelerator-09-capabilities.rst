.. "=  - # ` : ' " ~ ^ _ *  < >"

Capabilities of the Accelerator
===================================

**Business-minded**

    All data are stored to enable an ex-post analysis

**Cost-effective**

    The software developer accesses all data using a straightforward interface independently of the storage

**Customization**

    Implement any business rule on the data
    
    Add new data source at any time
    
    Choose the interfaces of the REST APIs

**Extensible**

    Add new functionalities to the ETL process

**Scalable**

    Scrap as many websites as you want

    Add as many data as you want

**High-performance**

    The Accelerator starts servers when necessary to support the load

**Secure**

    Authentication and Identification

    All connections between the components are secured

**Optimal**

    Cache the data in memory to fasten the process

**Fault-tolerant**

    Historical data are stored for future analysis

    Recovery of lost data

**Source code**

    Build by a Belgian company, owner of the source code

    The Accelerator relies on Open source Code and a proprietary Cloud Infrastructure
