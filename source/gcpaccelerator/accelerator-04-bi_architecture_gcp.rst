.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst

Google Cloud Architecture
===================================

The accelerator uses the :abbr:`GCP (Google Cloud Platform)` infrastructure : 

* :term:`Cloud Firestore`
* :term:`App Engine`
* :term:`Cloud Datastore`
* :term:`Cloud Dataflow`
* :term:`BigQuery`
* :term:`Cloud Datastore`
