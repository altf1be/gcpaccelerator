.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst

What are the possible Web Data Extraction Use Cases?
======================================================

Here is a non-exhaustive list of use cases the :term:`Web Scraping` has proven to be a useful solution.

.. list-table:: Web Data Extraction Use Cases - What kind of project are you working on?
   :widths: 33 33 34
   :header-rows: 0

   * - Market research
     - Price intelligence
     - Lead generation
   * - Brand monitoring
     - An alternative data source for the finance industry
     - Recruitment
   * - Business automation
     - :term:`MAP violations`
     - Fraud detection
