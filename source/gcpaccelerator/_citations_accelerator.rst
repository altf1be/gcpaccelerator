.. "= - # ` : ' " ~ ^ _ * < >"

Citations
==========

.. [BiddersEdge100FSupp2d105800] eBay, Inc. v. Bidder's Edge, Inc., 100 F. Supp. 2d 1058 (N.D. Cal. 2000). (n.d.). Retrieved from https://law.justia.com/cases/federal/district-courts/FSupp2/100/1058/2478126

.. [BiGartner12] Bittman, T. (2012, September 24). Mind the Gap: Here Comes Hybrid Cloud. Retrieved from https://blogs.gartner.com/thomas_bittman/2012/09/24/mind-the-gap-here-comes-hybrid-cloud


.. [DmlpNotCopy14] Works Not Covered By Copyright . (n.d.). Retrieved March 21, 2020, from https://www.dmlp.org/legal-guide/works-not-covered-copyright

.. [KO2018] Knoblock, C. A. (2018, February 3). Modeling Web Sources for Information Integration. Retrieved March 26, 2020, from https://github.com/usc-isi-i2/usc-isi-i2.github.io/blob/master/slides/2018-02-03-AAAI-KG-Tutorial-CK.pptx

.. [ScrapinghubBestPractices18] Scrapinghub. (n.d.). Guide to Web Scraping Best Practices. Retrieved March 21, 2020, from https://info.scrapinghub.com/web-scraping-guide/web-scraping-best-practices

.. [WebScrapingforCPI18] Web scraping and online data collection and processing for the consumer price index. (2018, February 8). Retrieved from https://statbel.fgov.be/en/news/web-scraping-and-online-data-collection-and-processing-consumer-price-index 

.. [WikiWebScraping20] Wikipedia contributors. (2020, March 11). Web scraping. In Wikipedia, The Free Encyclopedia. Retrieved 11:16, March 19, 2020, from https://en.wikipedia.org/w/index.php?title=Web_scraping&oldid=945118241

