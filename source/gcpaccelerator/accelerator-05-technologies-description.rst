.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst

Technologies used by the Accelerator
=========================================================

Wrappers for Web Data Extraction
----------------------------------------------

As the quantity and diversity of the information available online increases, more of the typical information access tasks are done by a program such as web wrappers. 

:term:`Wrappers` *"facilitate access to Web-based information sources by providing a uniform querying and data extraction capability."* [KO2018]_

.. sidebar:: Why are web scrapers useful?

    |why_are_web_scrapers_useful.png|
    
For example, a Web wrapper for the e-commerce website source can take a query for a Product and extract its description in the same way as the information is extracted from a database:

* the price or anterior price, 
* the promotion price, 
* the description, 
* the date when the data were collected, 
* and the bar code.
