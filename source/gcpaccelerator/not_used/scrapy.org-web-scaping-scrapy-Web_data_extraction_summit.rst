.. "= - # ` : ' " ~ ^ _ * < >"

Web Data Scraping Summit - best practices
==========================================

Web data extraction in 2019
----------------------------

Demand is Increasing

* Consumers are data literate
* Diverse use cases

Web data is business critical

* Quality
* Reliability
* Compliance

Web scraping is harder 

* complext websites
* Anti-bot
* Increasingly larger scale

Tools are improving

* Becoming specialised
* Automation is imminent

Web data Extraction
--------------------

www -> Proxy -> Download web data -> URL Discovery -> Data extraction -> Integration with ETL
URL Discovery> Queue -> Download Web Data

Types of crawl
----------------

* single website : custom crawler developed for a single website
* broad crawl : crawler for given type of data that is supplied a curated list of websites
* focussed crawl : discovers and crawls websites for a specific topic
* full web crawl : discocers and crawls every website

Crawl and Extraction techniques
--------------------------------

* manual : website-specific software
* visual : point-and-click application to build web scrapers
* generic : no per-website work, relies on machine learning

Post-extraction
----------------

* additional Quality Assurance (QA)
* classification



Source: 
------------------

* https://extractsummit.io/web-data-extraction-summit-videos-2019-attendees

* https://extractsummit.io/web-data-extraction-summit-videos-2019-attendees/?__hstc=251652889.acaa531c17e08a7ff9a6166c6ad8290d.1580663853872.1580663853872.1580663853872.1&__hssc=251652889.1.1580663853873&submissionGuid=d93bac2a-211d-416a-98bb-0043858fdc71

* Cathal Garvey <https://import.io>