.. "=  - # ` : ' " ~ ^ _ *  < >"

Quality Assurance System
===================================

At a high level, the goal of a webscraping QA system is to assess the quality/correctness of your data along with the coverage of the data you have scraped.

Data Quality and Correctness
    Verify that the correct data has been scraped (fields scraped are taken from the correct page elements).
    Where applicable, the data scraped has been post-processed and presented in the format requested by you during the requirement collection phase (e.g. formatting, added/stripped characters, etc.).
    The field names match the intended field names stipulated by you.

Coverage
    Item coverage - verify that the all available items have been scraped (items are the individual
products, articles, property listings, etc.).
    Field coverage - verify that all the available fields for each item have been scrape

When scraping at any reasonable scale, to achieve these goals of obtaining high data quality and coverage from your web scraping it is very important that you incorporate a multi-layer QA process into your web scraping projects which combines both automatic and manual tests to verify data quality.

At ALT-F1 we apply a four-layer QA process to our projects to ensure we are able to give our clients the highest quality data and coverage they require.

* Layer 1 - Pipelines
* Layer 2 - AccelMon
* Layer 3 - Manually-Executed Automated QA
* Layer 4 - Manual/Visual QA

.. graphviz:: /gcpaccelerator/accelerator-quality_assurance_system.dot

Layer 1 - Pipelines
---------------------

Item pipelines are rule-based :term:`scrap-center` constructs built into the extraction spider that are designed to process data as it is being scraped.

In the case of quality assurance, these pipelines are often used to:

* Cleanse and normalise the data (remove non-printable characters, convert to unicode, etc.).
* Validate scraped data (checking that the items contain pre-determined key fields, and dropping any that don’t, if so desired).
* Check for duplicates (and dropping them if so desired).
* Store the scraped items in a database and file systems

Layer 2 - AccelMon
----------------------

 The amount of items extracted by the spider.

* The amount of successful responses received by the spider.
* The amount of failed responses (server-side errors, network errors, proxy errors, etc.).
* The amount of requests that reach the maximum amount of retries and are finally discarded.
* The amount of time it takes to finish the crawl.
* The amount of errors in the log (spider errors, generic errors detected by Scrapy, etc.).
* The amount of bans.
* The job outcome (if it finishes without major issues or if it is closed prematurely because it detects too many bans, for example).
* The amount of items that don’t contain a specific field or a set of fields.

The amount of items with validation errors (missing required fields, incorrect format, values that don’t match a specific regular expression, strings that are too long/short, numeric values that are too high/low, etc.).

Again, Spidermon is a development activity, however, this time there is often QA input, with QA Engineers
regularly defining the validation to be performed and often implementing it themselves.