.. "=  - # ` : ' " ~ ^ _ *  < >"

Gabygael - 1er projet
=========================

GPI : Gabygaël Pirson
ABO : Abdelkrim Boujraf

Questions auxquelles il faut répondre
------------------------------------------

ACTION pour GPI
ACTION pour ABO

- Nom du client (GPI)

adrien chassaing
Founder & CEO

goal of the project
--------------------

récupérer des données avec les ordonnances

dosalogic
1. https://www.volumatik.com

1. REST API POST

- Numéro de TVA du client (ABO)
- Code nace du client (ABO)

- Produit à vendre (GPI)
- Coûts variables (GPI) : technologies nécessaires et capacités

- Quel subside pour ce projet? (ABO) 
- Ecrire l'offre pour lundi 2 Déc 2019-12-02 (ABO)

Sales du produit de gestion de big data
---------------------------------------

+ vente 400 eur/mois/utilisation (jusqueà 10.000 requêtes)
+ XXXX EUR supplémentaire par tranche de consommation 10K-40k 

ACTION : pour lundi 2 Décembre 2019-12-02, écrire l'offre


Notes prises pendant le call 28 Nov 2019-11-28
----------------------------------------------

Value proposition
#################

Slogan : GPI fédère les info.

Trois (3) produits à disposition: 

1. Gérer big data
    - Entre 13h et 17h : 40.000 requêtes
2. Outil de gestion de monitoring
3. Scrap (raspberry)
    - immovlan est désormais crawlable

prospect retail
###############

Au luxembourg

- nourriture - http://www.amexio.fr/en/home/

à Bruxelles

- 

Technology
##########

- redis (QUESTION: doit-il utiliser Redis dans le futur?)



Value proposition
#################

Technologies nécessaires google : 
- App engine
- Datastore
- Redis ( facultatif )
- Bigquery
- Cron task
- Storage 
- firebase

En général le cout serveur démarre à 50 euros +- et peux monter jusque 150 pour une grosse consommation.
Par contre si il faut plus de performance je peux doubler rapidement les machines ( ou plus ) et le cout double aussi. 
( 100 - 300 ). Le prix est par mois. 
Mais l’auto scaling est mis par défaut dans mon mode opératoire. Donc ça ne devrait pas être fait. 
Juste à prendre en considération pour le prix. 


Produit à vendre : 
1) API qui permet de recevoir et restituer les données en grande quantitée.  Une api de base ( GET, PUT, POST, DELETE, GET MULTI, PUT MULTI, DELETE MULTI ) est fournie de base pour tout « object » simple ( nom object + attributs ) indiqué par défaut. 
Pour toute autre api de plus haut niveau et spécifique à un besoin. Supplément pq il faut la construire. 

2) Une administration de management de ces objet « simple » via une interface super_admin. 

3) Un espace pour interface web custom si demandé par le client. Permet d’interagir directement avec le système sans passer par l’api. Elle peut être construite à la demande via un devis avantageux. 

4) Une gestion des authentifications user ( recovery password, register, sign_in sign_out ) via api et/ou liée au produit. Authentification basée sur firebase. 

En résumé au minimum un client peut: 

Commander un espace de stockage de fichier, data etc. Nous les envoyer et les demander sans limite de quantité de données ou de fréquence de requête. 

Au maximum : 
Nous confier son projet de A à Z en profitant de notre framework. 





EONIX

paul vigneron
creos
http://eonix.be/
https://www.cecp.be/creos/
député visar michel


gabygaël

https://www.cecp.be
https://www.cecp.be/creos
https://gesvesecolo.be/tag/michele-visart
http://eonix.be/consultance-2


aloysdubois 
adb@eonix.be

2020-02-20 CONF call
===================================

Comment protéger son code
-------------------------------

- ACTION : partager la closed license à mettre dans la root de tout code de Gabygaël
    - https://gist.github.com/Abdelkrim/0fd7ca47966bbfde09f719c4f74fcf2e

- refactoriser le code pour éviter les risques de croyance de copie de code non autorisée

- il faut séparer les repository de Gabygaël et ceux des clients
- Aucun code produit par les équipes de MMD ne doit se trouver dans le code de Gabygaël
- Garder une preuve des dessins d'architecture dessinés par Gabygaël
- PRICE est un projet créé par MDD car Gabygaël en a initialement décrit l'architecture
- Gabygaël ne facture pas 100% des développements des fonctionnalités car Immoffice profite des développements de la plateforme
- ACTION : regarde le code nace et la description des activités dans les statuts de MMD
- ACTION : regarde le code nace et la description des activités dans les statuts de Gabygaël
- ACTION : regarde le code nace et la description des activités dans les statuts de Immoffice RP SPRL

- ACTION : partager l'architecture de code pour gérer les semver et générer les CHANGELOG
    - commit-msg : https://gist.github.com/Abdelkrim/8b0c5bfa0ed8a6c651d71ec5f920e41d
    - package.json : https://gist.github.com/Abdelkrim/9ab435f61829e73cf729115561a026b0

Gabygaël a développé les projets suivants qui sont utilisés par MMD :

- data workspace pour zone
- data subscription
- trends extract
- prim product
- price feed
- Prem POS
- Data user
