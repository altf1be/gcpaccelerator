.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst

The Accelerator
==========================

The Accelerator is an IT infrastructure able to collect and analyze a massive amount of public data on the World Wide Web.

|generic_framework_web_data_extraction.png|

The Accelerator leverages the untapped potential of web data with the first solution designed for diverse sectors, completely scalable, available on-premise, and cloud-provider agnostic.

The Hybrid cloud infrastructure
-----------------------------------

The IT Infrastructure supporting the objective of the Accelerator is an :term:`Hybrid cloud` solution, including : 

Several servers deployed :term:`on-premise` running the :term:`scrapers` that collect the raw data on the internet websites. Scrapers push those data to the Cloud architecture for large-scale analysis

Several servers deployed on the Public :term:`Cloud` :

    1. store and analyze a large volume of data 
    2. and give access to the raw data and the data analyzed by the Accelerator to third-parties through secured :term:`Web APIs`

How does the Accelerator work?
---------------------------------

.. graphviz:: /gcpaccelerator/accelerator-bi_business_process.dot

1. The user of the web application selects the website that she wants to scrap
2. The :term:`scrapers` request continuously the :term:`scrap-center` which URL the scrapers have to scrape
3. The :term:`scrap-center` shares to the :term:`scraper` from which URL it should download data
4. The :term:`scraper` performs its duties, e.g., to download the HTML from the URL, to take a screenshot of the HTML page, to download the PDF available on the page
5. The :term:`scraper` uploads the collected data to the :term:`scrap-center`
6. The :abbr:`ETL (Extract-Transform-Load)` process starts including the :term:`Data Cleansing` and Data transformation by the :term:`scrap-center`
7. The :term:`scrap-center` 

    - stores the raw data into the :term:`data lake`
    - extracts and stores the metadata with a low velocity, that does not often change, into the :term:`Cloud Datastore` e.g., the product name, the URL where the product is available
    - stores the screenshot on a file system
    - stores the data with a high velocity, that do often change, up to near real-time to the :term:`data warehouse`, e.g., the product price, the promotion price

Now the data are available to the end-user.

8. The user selects the dashboard of the report she wants to analyze or print
9. The Web application generates the report based on the data stored in the :term:`data lake`, :term:`data warehouse` and the :term:`Cloud datastore`

The design of the BI Architecture
----------------------------------

.. graphviz::  /gcpaccelerator/accelerator-bi_architecture_hybrid_cloud.dot
