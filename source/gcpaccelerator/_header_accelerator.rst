.. the header.rst contains shared information amongst the documentation pages

.. _Google regions locations in Europe: https://cloud.google.com/about/locations#europe

.. _CCPA GDPR Chart By Thomson Reuters: https://www.bakerlaw.com/webfiles/Privacy/2018/Articles/CCPA-GDPR-Chart.pdf

.. _LinkedIn Data Scraping Ruled Legal: https://www.forbes.com/sites/emmawoollacott/2019/09/10/linkedin-data-scraping-ruled-legal

.. _Web scraping and online data collection and processing for the consumer price index: https://statbel.fgov.be/en/news/web-scraping-and-online-data-collection-and-processing-consumer-price-index

.. _Ryanair contre Opodo: https://www.legalis.net/jurisprudences/cour-dappel-de-paris-pole-5-chambre-2-arret-du-23-mars-2012

.. _Le scraping est-il légal?: https://lexing.be/le-scraping-est-il-legal

.. _Jurisprudence e-commerce \: Cour d’appel de Paris Pôle 5, chambre 2 Arrêt du 23 mars 2012: https://www.legalis.net/jurisprudences/cour-dappel-de-paris-pole-5-chambre-2-arret-du-23-mars-2012

.. _Jurisprudence E-Commerce \: Tribunal de Grande Instance de Paris 3ème chambre, 2ème section Jugement du 09 avril 2010: https://www.legalis.net/jurisprudences/tribunal-de-grande-instance-de-paris-3eme-chambre-2eme-section-jugement-du-09-avril-2010

.. _Facebook: https://www.facebook.com

.. _Jurisprudence E-Commerce Tribunal de commerce de Paris Ordonnance de référé 9 novembre 2007: https://www.legalis.net/jurisprudences/tribunal-de-commerce-de-paris-ordonnance-de-refere-9-novembre-2007/

.. _Ryanair: https://www.ryanair.com

.. _Opodo: https://www.opodo.com

.. _Amadeus: https://amadeus.com/en

.. _Ryanair and Momondo fall out over links to fare screenscapers: https://www.phocuswire.com/Ryanair-and-Momondo-fall-out-over-links-to-fare-screenscapers


.. _Norman Neyrinck: https://lexing.be/avocat/norman-neyrinck

.. _Arrêt n° 168 du 10 février 2015 (12-26.023): https://www.courdecassation.fr/jurisprudence_2/arrets_publies_2986/chambre_commerciale_financiere_economique_3172/2015_6949/fevrier_6951/168_10_31152.html

.. _eBay: https://www.ebay.com

.. _EBay v. Bidder's Edge: https://en.wikipedia.org/wiki/EBay_v._Bidder%27s_Edge

.. _eBay, Inc. v. Bidder's Edge, Inc., 100 F. Supp. 2d 1058 (N.D. Cal. 2000): https://law.justia.com/cases/federal/district-courts/FSupp2/100/1058/2478126

.. _scrapinghub: https://scrapinghub.com

.. _la convention de Berne: https://www.wipo.int/treaties/fr/ip/berne

.. _United States Court of Appeals for the Ninth Circuit: https://en.wikipedia.org/wiki/United_States_Court_of_Appeals_for_the_Ninth_Circuit 

.. _hiQ Labs, Inc. v. LinkedIn Corp., No. 17-16783 (9th Cir. 2019): _https://law.justia.com/cases/federal/appellate-courts/ca9/17-16783/17-16783-2019-09-09.html

.. _hiQ Labs v. LinkedIn: https://en.wikipedia.org/wiki/HiQ_Labs_v._LinkedIn

.. _hiQ Labs:  https://www.hiqlabs.com

.. _LinkedIn To Ask Supreme Court To Intervene In Scraping Battle With HiQ: https://www.mediapost.com/publications/article/343432/linkedin-to-ask-supreme-court-to-intervene-in-scra.html

.. _LinkedIn calls out hiQ for ‘illegally’ scraping their site: https://www.onmsft.com/news/linkedin-calls-out-hiq-for-illegally-scraping-their-site

.. _US court says scraping a site without permission isn’t illegal: https://thenextweb.com/security/2019/09/10/us-court-says-scraping-a-site-without-permission-isnt-illegal/

.. _LinkedIn, It\’s illegal to scrape our website without permission: https://arstechnica.com/tech-policy/2017/07/linkedin-its-illegal-to-scrape-our-website-without-permission

.. _LinkedIn: https://www.linkedin.com

.. _CAN-SPAM Act of 2003: https://en.wikipedia.org/wiki/CAN-SPAM_Act_of_2003

.. _Computer Fraud and Abuse Act: https://en.wikipedia.org/wiki/Computer_Fraud_and_Abuse_Act

.. _hiQ Skill Mapper: https://www.hiqlabs.com/new-skill-mapper

.. _hiQ Keeper: https://www.hiqlabs.com/new-keeper

.. _WarGames: https://en.wikipedia.org/wiki/WarGames

.. _Facebook, Inc. v. Power Ventures, Inc.: https://en.wikipedia.org/wiki/Facebook,_Inc._v._Power_Ventures,_Inc.

.. _Ruling for the case Facebook Inc. vs. Power Ventures Inc.: https://www.documentcloud.org/documents/2991898-13-17102.html

.. _Startup that we all forgot gets small win against Facebook on appeal: https://arstechnica.com/tech-policy/2016/07/startup-that-we-all-forgot-gets-small-win-against-facebook-on-appeal


.. _California Comprehensive Computer Data Access and Fraud Act: https://en.wikipedia.org/wiki/California_Comprehensive_Computer_Data_Access_and_Fraud_Act

.. _Digital Millennium Copyright Act: https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act

.. _Convention de Paris pour la protection de la propriété industrielle: https://fr.wikipedia.org/wiki/Convention_de_Paris_pour_la_protection_de_la_propri%C3%A9t%C3%A9_industrielle 

.. _Trademark infrigement: https://en.wikipedia.org/wiki/Trademark_infringement

.. _Data scrapen van het internet\: mag dat wel?: https://www.tijd.be/tech-media/media-marketing/data-scrapen-van-het-internet-mag-dat-wel/9999434.html

.. _SNCB: https://en.wikipedia.org/wiki/National_Railway_Company_of_Belgium

.. _NMBS: https://en.wikipedia.org/wiki/National_Railway_Company_of_Belgium

.. _stratégie fédérale open data: https://data.gov.be

.. _IRail: https://www.irail.be

.. _Le webscraping, la collecte et le traitement de données en ligne pour l'indice des prix à la consommation: https://statbel.fgov.be/sites/default/files/files/documents/Analyse/FR/Webscraping_fr.pdf

.. _Webscraping, gegevens online verzamelen en verwerken voor de consumptieprijsindex: https://statbel.fgov.be/sites/default/files/files/documents/Analyse/NL/webscraping_nl.pdf

.. _forme galénique: https://fr.wikipedia.org/wiki/Forme_gal%C3%A9nique

.. _CNK: https://www.apb.be/nl/corp/De-Algemene-Pharmaceutische-Bond/Ontdek-onze-diensten/De-CNK-code/Pages/CNK-code-informatie-brochure.aspx

.. _EAN: https://en.wikipedia.org/wiki/International_Article_Number

.. _CIP7: https://www.ansm.sante.fr/Activites/Elaboration-de-bonnes-pratiques/Codification-et-tracabilite-des-medicaments/(offset)/10#paragraph_23984

.. _CIP13: https://www.ansm.sante.fr/Activites/Elaboration-de-bonnes-pratiques/Codification-et-tracabilite-des-medicaments/(offset)/10#paragraph_23984

.. |accelerator-website-setup_for_dreamland.be.png| image:: /_static/accelerator/admin_ui/accelerator-website-setup_for_dreamland.be.png
                    :alt: accelerator : configuration required to scrap dreamland.be
                    :width: 600 px

.. |accelerator-list_of_websites_to_scrap.png| image:: /_static/accelerator/admin_ui/accelerator-list_of_websites_to_scrap.png  
                    :alt: accelerator : list of websites to scrap
                    :width: 600 px


.. |accelerator-01-alert-choose-a-website.png| image:: /_static/accelerator/eec-268509.appspot.com/01-alert-choose-a-website.png  
                    :alt: accelerator : Choose the e-commerce website
                    :width: 600 px

.. |accelerator-03-show-alerts-from-all-websites.png| image:: /_static/accelerator/eec-268509.appspot.com/03-show-alerts-from-all-websites.png  
                    :alt: accelerator : Show alerts from all websites
                    :width: 600 px

.. |accelerator-05-show-alerts-from-one-website.png| image:: /_static/accelerator/eec-268509.appspot.com/05-show-alerts-from-one-website.png 
                    :alt: accelerator : Show alerts from one website
                    :width: 600 px

.. |accelerator-07-show-a-product-details-from-company-manipulating-its-price.png| image:: /_static/accelerator/eec-268509.appspot.com/07-show-a-product-details-from-company-manipulating-its-price.png 
                    :alt: accelerator : Show a product details from a company failing to apply the Union consumer protection rules
                    :width: 600 px


.. |accelerator-09-dreamland-show-category.png| image:: /_static/accelerator/eec-268509.appspot.com/09-dreamland-show-category.png 
                    :alt: accelerator : Show alerts from one website
                    :width: 600 px

.. |generic_framework_web_data_extraction.png| image:: /_static/gif/medium.com/generic_framework_web_data_extraction.png  
					:alt: From web data extraction to structured data  - credits: medium.com

.. |why_are_web_scrapers_useful.png| image:: /_static/gif/hackernoon.com/why-are-web-scrapers-useful.png 
					:alt: Why are web scrapers useful? - credits: hackernoon.com

.. |data_extraction.png| image:: /_static/gif/visualwebripper.com/data_extraction.png 
					:alt: From a Web Data Extraction to structured data - credits: visualwebripper.com