.. "=  - # ` : ' " ~ ^ _ *  < >"

.. include:: /gcpaccelerator/_header_accelerator.rst


Research project
==================================

Projects
-----------------

Adaptative crawling
##########################

https://dl.acm.org/doi/10.1145/2492517.2492624



Ideas
---------------------


- optimiser le scrap dépendant aux premières trouvailles statistiques

- optmise the frequency of teh scriping depending on the time of the day
    - prices change during the day (lunch time, night)

- Plugin firefox.
    - scraper autonome
    - Identification produit affiche et comparateur prix du produit courant.
    - Plugin firefox. Envoi page contenu sur page affichée 
 
- Php SQL
    - Générateur de configuration automatique sur base des configs existante.

- Analyseur comparateur image. 
- Analyseur comparateur contenu texte ( HTML ) 


Tools
------------------

Source : "Data mining in unusual domains with information-rich knowledge graph construction, inference and search", Mayank Kejriwal, Pedro Szekely, Information Sciences Institute, USC Viterbi School of Engineering, KDD2017, 2017-08-13-kdd-tutorial-kejriwal-szekely.pdf 

Scrapy (targeted crawling) - Open source and collaborative framework for extracting data from websites - https://scrapy.org/ACHE
- Apache Nutch (massive crawling) - highly extensible, highly scalable Web crawler - http://nutch.apache.org
- Deep Deep (Adaptive crawler) reinforcement learning to learn which links to follow - https://github.com/TeamHG-Memex/deep-deep
- ACHE (focused crawler) : https://github.com/ViDA-NYU/ache
- ScrapingHub (service) : https://scrapinghub.com/data-on-demand

- Inferlink Extractor - Automatic extraction from semi-structured pages https://github.com/inferlink
