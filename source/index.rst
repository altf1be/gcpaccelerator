.. "= - # ` : ' " ~ ^ _ * < >"


.. include:: /gcpaccelerator/_header_accelerator.rst

The Accelerator - BI Architecture to turn websites pages into insightful data
===================================================================================

.. toctree::
      :maxdepth: 3
      :numbered:
      
      gcpaccelerator/accelerator-01-introduction.rst
      gcpaccelerator/accelerator-03-bi_architecture.rst
      gcpaccelerator/accelerator-04-bi_architecture_gcp.rst
      gcpaccelerator/accelerator-05-technologies-description.rst
      gcpaccelerator/accelerator-07-use_cases.rst
      gcpaccelerator/accelerator-09-capabilities.rst
      gcpaccelerator/_citations_accelerator.rst
      gcpaccelerator/_glossary_accelerator.rst 
      license-closed.rst
