.. "= - # ` : ' " ~ ^ _ * < >"


.. include:: /gcpaccelerator/_header_accelerator.rst 

.. _Considérations légales et morales de l'extraction de données (scraping): 

Considérations légales et morales de l'extraction de données (scraping)
=========================================================================

.. note::
    Nous relatons ici une analyse effectuée par ALT-F1 qui est en cours mais qui n'a pas été validée par un ou une juriste.

Beaucoup de littérature existe concernant le :term:`Web scraping` mais peut d'écrits s'aventurent à trancher le débat : le scraping est-il légal ou non ?

L'institut belge des statistiques a publié un guide [WebScrapingforCPI18]_ qui décrit l'utilisation du :term:`Web Scraping` dans le calcul de l’indice des prix à la consommation mais qui ne mentionne le côté légal ou non du Web Scraping : 

* `Le webscraping, la collecte et le traitement de données en ligne pour l'indice des prix à la consommation`_
* `Webscraping, gegevens online verzamelen en verwerken voor de consumptieprijsindex`_


Textes législatifs européens et Conventions internationales concernant le droit d'auteur
------------------------------------------------------------------------------------------

La Directive (UE) 2019/790 (:2019_790:`FR` :2019_790:`NL`) du Parlement européen et du Conseil du 17 avril 2019 sur le droit d'auteur et les droits voisins dans le marché unique numérique et modifiant les directives 96/9/CE et 2001/29/CE.

    L'article 1 définit la directive comme suit : *"La présente directive fixe des règles visant à poursuivre l'harmonisation du droit de l'Union applicable au droit d'auteur et aux droits voisins dans le cadre du marché intérieur, en tenant compte, en particulier, des utilisations numériques et transfrontières des contenus protégés ... ainsi que des règles destinées à assurer le bon fonctionnement du marché pour l'exploitation des œuvres et autres objets protégés."* 

    La directive se focalise sur la copie de données protégées par le droit d'auteur comme les *"publication de presse"* publiées sur des sites publics par des *"fournisseur de services de partage de contenus en ligne"* tels que Google, Facebook, Amazon, Apple ou Microsoft.

    La directive permet le :term:`text mining` ou :term:`data mining` pour des raisons scientifiques, *"fouille de textes et de donnée"* en français dans le texte.

La `Convention de Paris pour la protection de la propriété industrielle`_, publiée le 20 Mars 1883 dont la dernière modification était le 28 Septembre 1979, a pour *"objet [la protection ] [d]les brevets d’invention, les modèles d’utilité, les dessins ou modèles industriels, les marques de fabrique ou de commerce, les marques de service, le nom commercial et les indications de provenance ou appellations d’origine, ainsi que la répression de la concurrence déloyale"* connue plus généralement sous le nom anglais de `Trademark infrigement`_.

Les faits

    L'extraction de données effectuées par nos logiciels de :term:`Web Scraping` extraient principalement ces éléments : 
    
    - la copie textuelle ("contenu brut") de la page HTML sur laquelle se trouve le produit 
    - une copie écran de la page qui décrit le produit
    - l'url sur laquelle le produit est accessible
    
    Une analyse de type (:term:`text mining`) du contenu brut est effectuée pour en extraire les faits, par exemple : 
    
    - le nom du produit 
    - le prix du produit
    - la promotion effectuée sur le produit
    - l'heure à laquelle l'extraction a été effectuée
    
    *"La législation sur le copyright permet d'utiliser les faits ("facts" en anglais) car ils ne sont pas protégés par le droit d'auteur"* [DmlpNotCopy14]_ comme le renforcerait `la convention de Berne`_ qui porte sur la protection des œuvres et des droits des auteurs sur leurs œuvres. Le droit d'auteur s'applique uniquement au travail original de nature *"toutes les productions du domaine littéraire, scientifique et artistique, quel qu'en soit le mode ou la forme d'expression"*.
    Bien que le niveau de créativité requis pour être une œuvre "originale" est extrêmement bas, les faits n'atteignent pas le niveau de créativité requis : 
    
    - Un score de match de football, un numéro de téléphone, une date de naissance, le nombre de personnes participants à une manifestation ne sont pas protégeables par le droit d'auteur.

    Enfin, d'après le site `scrapinghub`_, dont l'objectif est d'extraire des données à la demande, le nom d'un produit, son prix, ses caractéristiques sont qualifiés de faits et ne sont pas protégés par le droit d'auteur [ScrapinghubBestPractices18]_. 

Textes législatifs américains utilisés lors de procès liés à l'extraction de données (Web Scraping)
---------------------------------------------------------------------------------------------------------

La loi :abbr:`CAN-SPAM (Controlling the Assault of Non-Solicited Pornography And Marketing)` (`CAN-SPAM Act of 2003`_) qui signifie en anglais : "canning" (mettre un terme au) spam.


La loi :abbr:`CCCDAFA (California Comprehensive Computer Data Access and Fraud Act)` (`California Comprehensive Computer Data Access and Fraud Act`_) offre une protection aux individus, aux entreprises et aux agences gouvernementales contre la falsification, les interférences, les dommages et l'accès non autorisé aux données informatiques et aux systèmes informatiques créés légalement. Il permet une action civile contre toute personne reconnue coupable d'avoir violé les dispositions pénales en matière de dommages-intérêts compensatoires

La loi :abbr:`CFAA (Computer Fraud and Abuse Act)` (`Computer Fraud and Abuse Act`_). Cette loi interdit tout accès à un ordinateur sans autorisation préalable ou tout accès qui excède les autorisations.

La loi :abbr:`DMCA (Digital Millennium Copyright Act)` (`Digital Millennium Copyright Act`_) est une loi américaine adoptée en 1998. Le but de ce texte est de fournir un moyen de lutte contre les violations du droit d'auteur. La loi criminalise la production et la diffusion de technologies, d'appareils ou de services destinés à contourner les mesures qui contrôlent l'accès aux œuvres protégées par le droit d'auteur (communément appelées gestion des droits numériques ou :term:`DRM`). Il criminalise également l'acte de contournement d'un contrôle d'accès, qu'il y ait ou non violation réelle du droit d'auteur lui-même.

Jurisprudence
-----------------------

Est-il légal d'extraire les données publiques ? 

Depuis l'année 2000, plusieurs procès ont été conclus entre des sociétés qui détenaient des bases de données dont elles étaient les auteurs ("auteurs") et des sociétés qui indexaient ou collectaient les données produites par ces "auteurs". Les procès ses sont soldés par ces conclusions :  "la société (scraper) doit arrêter l'extraction des données (scraping)", "Le Web scraping est licite" ou "il n'est pas interdit de faire du Web Scraping".


EBay vs. Bidder's Edge (Le demandeur gagne le procès en 2000)
##############################################################

C'est en 2000 que l'affaire `EBay v. Bidder's Edge`_ **interdit la société Bidder's Edge d'extraire les données de la société `eBay`_**. Bidder's Edge a été sommée d'arrêter d'extraire les données de la société EBay en vue de les indexer grâce à un :term:`Web crawler` : *"Bidder's Edge, ... are hereby enjoined pending the trial of this matter, from using any automated query program, robot, web crawler or other similar device, without written authorization, to access eBay's computer systems or networks, for the purpose of copying any part of eBay's auction database"* [BiddersEdge100FSupp2d105800]_.


Ryanair contre Vivacances et Opodo (Le demandeur perd le procès en 2007)
###########################################################################

Vivacances est un site internet créé en 2002 dont l’activité est composée aux deux tiers par la billetterie et pour le reste par les ventes tourisme. 

`Opodo`_ est une agence de voyages en ligne créée en 2001.

`Amadeus`_ est une société de droit espagnol créée en 1987 par quatre compagnies aériennes européennes (Air France, Iberia, Lufthansa et SAS) dans le but de créer une structure commune de distribution informatisée des segments aériens puis de regrouper l'ensemble de l'offre de vente de billets d'avion, toutes compagnies confondues.
Amadeus détient 100% de Opodo depuis 2004. Amadeus et Opodo détiennent 67,3% et 37,3% de Vivacances respectivement depuis 2005.

`Ryanair`_ est une compagnie aérienne à bas prix irlandaise fondée en 1984. 

Ryanair est déboutée le 9 Novembre 2007 pour plusieurs faits dont la plainte de copie substantielle du contenu de sa base de données par Vivacances (`Jurisprudence E-Commerce Tribunal de commerce de Paris Ordonnance de référé 9 novembre 2007`_).

*"Après une ordonnance de référé en date du 9 novembre 2007 par laquelle le Président du Tribunal de commerce de Paris l’a déboutée [Ryanair] de ses demandes en considérant qu’elle « ne subit pas de préjudice à voir ses billets d’avion vendus à des clients qui viennent par l’intermédiaire du site de Vivacances » ... [Ryanair] revendique ensuite le statut de producteur de base de données, et prétend que la société Opodo aurait extrait et reproduit une partie substantielle du contenu de cette base, ce qui constituerait une atteinte à ses droits en tant que producteur "*

* Novembre 9, 2007, `Jurisprudence E-Commerce Tribunal de commerce de Paris Ordonnance de référé 9 novembre 2007`_

SNCB/NMBS contre IRail (le demandeur gagne le procès en 2008)
###############################################################

La :abbr:`SNCB (Société nationale des chemins de fer belges)`, en néerlandais :abbr:`NMBS (Nationale Maatschappij der Belgische Spoorwegen)` est l'entreprise de droit public créée en 1926 qui gère l'infrastructure et exploite le réseau ferré belge.

`IRail`_ est `une suite d'applications open source <https://github.com/iRail>`_ mobile et sur ordinateur qui indique si le train qu'un passager désire prendre est comble ou non. Le passager peut donc décider de prendre le train précédent ou suivant (`<https://irail.be/spitsgids>`_).

Pieter Colpaert, étudiant à l'université de Gand, collecte les données (:term:`Web Scraping`) sur le site de la SNCB/NMBS car les données y sont plus à jour que via leur :term:`Web API`.
La SNCB/NMBS gagne son procès et demande à Mr Colpaert d'arrêter le :term:`Web Scraping` du site.

Mr Colpaert continue à supporter son application en mode "dégradé" en utilisant les données accessibles sur la :term:`Web API` jusqu'en Juillet 2015.

Depuis l'application de la `stratégie fédérale Open Data`_ belge du 24 Juillet 2015, la SNCB/NMBS est forcée de partager ses données à destination du public à toutes les applications tierces ; l'application IRail peut à nouveau accéder aux données de la SNCB/NMBS en temps réel et fournir les services qu'elle fournissait en 2008 !

* Le site Open Data de la SNCB/NMBS est accessible en :sncb_nmbs_open_data:`FR` et en :sncb_nmbs_open_data:`NL`
* `SNCB`_, `NMBS`_ on Wikipedia
* April 6, 2018 `Data scrapen van het internet\: mag dat wel?`_

Ryanair contre Opodo (Le demandeur perd le procès en 2015)
###############################################################

L'`Arrêt n° 168 du 10 février 2015 (12-26.023)`_ français rendu par la cour de cassation dans l'affaire `Ryanair contre Opodo`_ **rejette le pourvoi de Ryanair** car *la société Opodo n’a pas fait usage des signes litigieux pour vendre elle-même des services désignés par les marques, mais seulement pour désigner, de manière nécessaire, les services de transport aérien de la société Ryanair qu’elle proposait au consommateur, et retient que les signes sont reproduits, sans qu’il y ait confusion sur l’origine des services ni atteinte aux droits de marque de cette société, à titre d’information sur le nom de la compagnie ; qu’en l’état de ces constatations et appréciations, faisant ressortir que la société Opodo avait fait un usage licite des marques, la cour d’appel, qui a effectué la recherche prétendument omise visée à la troisième branche, a, abstraction faite du motif surabondant critiqué à la première branche et sans encourir le grief de la deuxième branche, légalement justifié sa décision ; que le moyen n’est pas fondé ;*

Ryanair a payé à Opodo plusieurs dommages dont un dommage pour concurrence déloyale *"en diffusant un communiqué de presse le 28 janvier 2008"*.

* `Le scraping est-il légal?`_ par `Norman Neyrinck`_ 
* April 9, 2010, `Jurisprudence E-Commerce \: Tribunal de Grande Instance de Paris 3ème chambre, 2ème section Jugement du 09 avril 2010`_
* Mars 23, 2012, `Jurisprudence E-commerce \: Cour d’appel de Paris Pôle 5, chambre 2 Arrêt du 23 mars 2012`_
* Fév. 10, 2015, `Arrêt n° 168 du 10 février 2015 (12-26.023)`_

Facebook contre Power Ventures (le demandeur perd le procès en 2016)
#########################################################################

Power Ventures créée un site internet qui permet à ses utilisateurs d'agréger sur un seul site les données des utilisateurs dispersées sur plusieurs sites de réseaux sociaux ou de sites de messageries incluant LinkedIn, Twitter, Myspace, AOL ou Yahoo instant messaging.
Power Ventures est maintenant en faillite et anciennement accessible sur power.com. 

Facebook est le réseau social américain créé en 2004 comptant 2,5 Milliards d'utilisateurs en 2020.

En 2008, la décision du `United States Court of Appeals for the Ninth Circuit`_ interdit la société Power Ventures de collecter les données des utilisateurs de `Facebook`_ car Power Ventures aurait enfreint la loi anti-spam américaine de :abbr:`CAN-SPAM (Controlling the Assault of Non-Solicited Pornography And Marketing)` de 2003 en envoyant des emails publicitaires aux membres de Facebook. 

Mais c'est le 12 Juillet 2016 que cette même cour, saisie de l'appel de Power Ventures, décide d'annuler la sentence précédente (`Startup that we all forgot gets small win against Facebook on appeal`_ ) et indique que Power Ventures avait le droit d'envoyer des emails aux membres de Facebook car un consentement clair leur avait été demandé avant de faire la publicité de leur produit, refusant donc la définition d'envoi d'emails publicitaires non sollicités (des spams) décrite par Facebook.

- `CAN-SPAM Act of 2003`_ on Wikipedia
- `Facebook, Inc. v. Power Ventures, Inc.`_ on Wikipedia
- July 12, 2016, `Startup that we all forgot gets small win against Facebook on appeal`_ 
- July 12, 2016, `Ruling for the case Facebook Inc. vs. Power Ventures Inc.`_ 


LinkedIn contre hiQ Labs (le demandeur perd le procès en 2019 mais est en appel)
#################################################################################

La cour américaine (`United States Court of Appeals for the Ninth Circuit`_), qui a juridiction en Californie, a rejeté l'appel de `LinkedIn`_ qui demandait l'arrêt de l'extraction des données publiques de son site par la société `hiQ Labs`_. `hiQ Labs`_ récolte les profils d'utilisateurs de `LinkedIn`_ et les utilise pour analyser les données sur les effectifs, par exemple en prédisant quand les employés sont susceptibles de quitter leur emploi, ou dans quels services des pénuries de compétences peuvent apparaître. Les deux produits de hiQ Labs incriminés sont `hiQ Keeper`_ and `hiQ Skill Mapper`_.

La particularité de la plainte réside dans le fait que LinkedIn se base sur la loi :abbr:`CFAA (Computer Fraud and Abuse Act)` pour définir l'infraction : "l'accès à un ordinateur sans autorisation ou le dépassement de l'accès autorisé", loi adoptée après que le film `WarGames`_ ait été diffusé en 1983 aux États-Unis. 
LinkedIn a perdu chaque procès jusqu'à présent mais un pourvoi en cassation est en cours car la cour indique que

- *"les profiles sont accessibles au public car les utilisateurs ont bien entendu l'intention qu'ils soient accessibles par d'autres"* 
- *"[l'information collectée par hiQ Labs n'est pas privée comme indiquée dans la loi américaine car] l'information [n'a pas été] délimitée [par LinkedIn] comme privée grâce à l'utilisation d'une condition d'autorisation quelconque"*

- `LinkedIn calls out hiQ for ‘illegally’ scraping their site`_ 
- `US court says scraping a site without permission isn’t illegal`_
- July 31, 2017, `LinkedIn, It\’s illegal to scrape our website without permission`_ 
- Sep 10, 2019, `LinkedIn Data Scraping Ruled Legal`_
- `Computer Fraud and Abuse Act`_ on Wikipedia
- Nov. 15, 2019, `LinkedIn To Ask Supreme Court To Intervene In Scraping Battle With HiQ`_
- `hiQ Labs v. LinkedIn`_ on Wikipedia

Les affaires en cours
########################

Voici une liste de liens vers des affaires en cours, cette liste évolue avec le temps : 

- Novembre 23, 2016, `Ryanair and Momondo fall out over links to fare screenscapers`_ 

.. note::
    Veuillez noter que les informations sont données à titre informatif. Les services juridiques du client doivent statuer sur le sujet. ALT-F1 ne peut pas prendre les responsabilités juridiques et refusera d'effectuer des scraping qui sont illégaux par essence (par ex : Extraire les données du site internet d'une banque en vue de trouver la liste de ses clients ou des transactions effectuées par ses clients)

.. Netiquette
        ---------------------------

