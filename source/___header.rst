.. the header.rst contains shared information amongst the documentation pages

.. |DoD-Enterprise-DevSecOps-Initiative-Introduction-v4.52.pptx| replace:: :download:`DoD Enterprise DevSecOps Initiative (Software Factory) <_static/software.af.mil/DoD-Enterprise-DevSecOps-Initiative-Introduction-v4.52.pptx>`
 
.. |CSC-Activity-Report-2019.pdf| replace:: :download:`Cyber Security Coalition - Activity report 2019 <_static/pdf/cybersecuritycoalition.be/CSC-Activity-Report-2019.pdf>`

.. E-Mails (sorted)

.. _Abdelkrim Boujraf: abo+business_models@alt-f1.be


.. URL Links (sorted)

.. _ALT-F1: http://www.alt-f1.be

.. Substitutions of Images (sorted)
