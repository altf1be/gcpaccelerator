All trademarks mentioned belong to their owners, third party brands, 
product names, trade names, 
corporate names and company names 
mentioned may be trademarks of their respective owners 
or registered trademarks of other companies and are used for purposes of 
explanation and to the owner's benefit, without implying a 
violation of copyright law.

Toutes les marques citées appartiennent à leurs propriétaires. Les marques de tiers, 
les noms de produits, les noms commerciaux, les dénominations sociales et les noms 
de sociétés mentionnés peuvent être des marques déposées de leurs propriétaires 
respectifs ou des marques déposées d’autres sociétés et sont utilisés à des fins 
d’explication et au profit de leurs propriétaires, sans impliquer une violation 
de la loi sur le droit d'auteur.
