.. "= - # ` : ' " ~ ^ _ * < >"

License
=============

.. include:: ___header_copyrights.rst

|copy| 2010-2020 Abdelkrim Boujraf, alt-f1 sprl <http://www.alt-f1.be>, Abdelkrim Boujraf <http://www.alt-f1.be/literature.html>

This work is licensed under a Creative Commons Attribution 4.0 International License: <http://creativecommons.org/licenses/by/4.0>

* Share — copy and redistribute the material in any medium or format 
* Adapt — remix, transform, and build upon the material for any purpose, even commercially

Vous êtes autorisé à <https://creativecommons.org/licenses/by/4.0/deed.fr>: 

* Partager : copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
* Adapter : remixer, transformer et créer à partir du matériel pour toute utilisation, y compris commerciale

.. include:: ___license-trademarks.rst
