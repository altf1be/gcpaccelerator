.. "= - # ` : ' " ~ ^ _ * < >"

License
===========

.. include:: ___header_copyrights.rst

|copy| 2019-2020 Abdelkrim Boujraf, ALT-F1 SPRL <http://www.alt-f1.be>

.. include:: ___license-trademarks.rst