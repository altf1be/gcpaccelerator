echo on
echo version 2018-12-01 
rd build\singlehtml /s /q
rd build\docx /s /q

make singlehtml && mkdir build\docx && cd build\singlehtml  && pandoc .\index.html -f html -t docx --reference-doc ..\..\templates\reference-la-boucherie.docx -o .\index.docx --verbose && move .\index.docx ..\docx\ && cd ..\..


echo 'finished!'ls -la